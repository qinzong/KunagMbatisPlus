package com.example.kuang.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.kuang.mybatis.plus.pojo.User;
import org.springframework.stereotype.Repository;

/*代表持久层*/
@Repository
public interface UserMapper extends BaseMapper<User> {
}
