package com.example.kuang.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KuangMybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(KuangMybatisPlusApplication.class, args);
    }

}
