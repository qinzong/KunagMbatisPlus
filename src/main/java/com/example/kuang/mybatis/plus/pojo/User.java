package com.example.kuang.mybatis.plus.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @TableId(value = "id",type = IdType.INPUT)
    private long id;
    private String username;
    private String account;
    private String password;

    /*乐观锁*/
    @Version
    private Integer version;

    /*逻辑删除*/
    @TableLogic
    private Integer deleted;

    @TableField(fill = FieldFill.INSERT)
    private Date crateTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
