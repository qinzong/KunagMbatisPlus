package com.example.kuang.mybatis.plus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.kuang.mybatis.plus.mapper.UserMapper;
import com.example.kuang.mybatis.plus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WrapperTest {
    @Autowired
    private UserMapper mapper;

    @Test
    void wrapperDemo(){
        /*查询用到的Wrapper*/
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        /*查询名字不为空*/
        wrapper.isNotNull("id");
        wrapper.like("username","老板");
        mapper.selectList(wrapper);
    }
}
