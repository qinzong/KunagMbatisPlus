package com.example.kuang.mybatis.plus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuang.mybatis.plus.mapper.UserMapper;
import com.example.kuang.mybatis.plus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class KuangMybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Test
    void contextLoads() {
        List list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }

    @Test
    void TestInset(){
        User user = new User();
        user.setUsername("梅老板");
        user.setAccount("122");
        user.setPassword("133");
        userMapper.insert(user);
    }

    @Test
    void TestUpdate(){
        User user = new User();
        user.setId(7);
        user.setUsername("贾斯汀 老嘿");
        user.setAccount("123");
        user.setPassword("321");
        userMapper.updateById(user);
    }

    /*查询单个*/
    @Test
    void selectOne(){
        User user = userMapper.selectById(7);
        System.out.println(user);
    }

    /*查询多个,删除也一样*/
    @Test
    void selectArr(){
        List<User> users = userMapper.selectBatchIds(Arrays.asList(7, 8));
        users.forEach(System.out::println);
    }

    /*按条件查询 Map  ,删除也一样*/
    @Test
    void selectCondition(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("username","贾斯汀 老嘿");
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }

    /*分页查询*/
    @Test
    void testPage(){
        Page<User> page = new Page<>(1, 5);
        userMapper.selectPage(page,null);
        page.getRecords().forEach(System.out::println);
    }

    /*逻辑删除*/
    @Test
    void deletedUser(){
        int i = userMapper.deleteById(14);
        System.out.println(i);
    }
}
